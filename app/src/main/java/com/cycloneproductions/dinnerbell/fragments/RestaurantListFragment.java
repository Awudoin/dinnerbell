package com.cycloneproductions.dinnerbell.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cycloneproductions.dinnerbell.R;
import com.cycloneproductions.dinnerbell.adapters.RestaurantAdapter;
import com.cycloneproductions.dinnerbell.sqlite.DBHelper;

public class RestaurantListFragment extends ListFragment {
    ListInterface callback;
    RestaurantAdapter adapter;
    DBHelper db;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ListInterface) {
            callback = (ListInterface)activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBHelper(this.getActivity());
        adapter = new RestaurantAdapter(this.getActivity(), db.getAllRests());
        this.setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.restaurant_list,container,false);
        return view;
    }

    @Override
    public void onListItemClick(ListView list, View view, int position, long id) {
        super.onListItemClick(list, view, position, id);
        callback.restSelected(id);
    }

    public static RestaurantListFragment newInstance() {
        RestaurantListFragment lf = new RestaurantListFragment();
        return lf;
    }

    public interface ListInterface {
        void restSelected(long id);
    }
}