package com.cycloneproductions.dinnerbell.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.cycloneproductions.dinnerbell.R;

public class SearchBarFragment extends Fragment {
    SearchBarInterface callback;

    EditText searchField;

    // Constructor that Android OS calls with API 23+
    @TargetApi(23)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof SearchBarInterface) {
            callback = (SearchBarInterface)context;
        }
    }

    // Constructor that Android OS calls with API 22-
    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof SearchBarInterface) {
            callback = (SearchBarInterface)activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_bar, container, false);

        //text for search button
        searchField = (EditText)view.findViewById(R.id.fieldLocation);

        //search button
        view.findViewById(R.id.buttonGo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchText = "";
                if (searchField.getText() != null) {
                    searchText = searchField.getText().toString();
                }
                callback.onSearchClick(searchText);
            }
        });
        return view;
    }

    public static SearchBarFragment newInstance() {
        return new SearchBarFragment();
    }

    public interface SearchBarInterface {
        void onSearchClick(String searchText);
    }
}