package com.cycloneproductions.dinnerbell.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.cycloneproductions.dinnerbell.R;

public class SortBarFragment extends Fragment {
    SortBarInterface callback;

    @Override
    public void onAttach(Context content) {
        super.onAttach(content);
        if (content instanceof SortBarInterface) {
            callback = (SortBarInterface)content;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sort_list,null,false);

        view.findViewById(R.id.btn_dist).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: add distance button sorting
                callback.onDistClick();
            }
        });
        view.findViewById(R.id.btn_wait).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: add wait button sorting
                callback.onWaitClick();
            }
        });
        view.findViewById(R.id.btn_abc).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: add alphabetical button sorting
                callback.onAbcClick();
            }
        });
        view.findViewById(R.id.btn_filter).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: add filter select
                callback.onFilterClick();
            }
        });
        return view;
    }

    public static SortBarFragment newInstance() {
        SortBarFragment sb = new SortBarFragment();
        return sb;
    }

    public interface SortBarInterface {
        void onDistClick();
        void onWaitClick();
        void onAbcClick();
        void onFilterClick();
    }
}