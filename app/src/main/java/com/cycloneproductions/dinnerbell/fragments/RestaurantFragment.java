package com.cycloneproductions.dinnerbell.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cycloneproductions.dinnerbell.R;
import com.cycloneproductions.dinnerbell.model.GoogleMapPlace;
import com.cycloneproductions.dinnerbell.sqlite.DBHelper;
import com.cycloneproductions.dinnerbell.submit.WaitParty;

public class RestaurantFragment extends Fragment  implements View.OnClickListener {
    RestaurantInterface callback;
    long restId;
    DBHelper db;

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof RestaurantInterface) {
            callback = (RestaurantInterface)context;
        }
        db = new DBHelper(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.restaurant_view, container, false);
        view.findViewById(R.id.btn_time).setOnClickListener(this);
        GoogleMapPlace place = db.getRest(restId);

        TextView name = (TextView) view.findViewById(R.id.textRestName);
        name.setText(place.getName());

        String ss[] = place.getFormattedAddress().split(", ");

        TextView address1 = (TextView) view.findViewById(R.id.textAddress1);
        TextView address2 = (TextView) view.findViewById(R.id.textAddress2);
        address1.setText(ss[0]);
        address2.setText(ss[1] + ", " + ss[2]);

        TextView time = (TextView) view.findViewById(R.id.textRestWaitTime);
        time.setText(String.valueOf(db.getAvgWaitTime(restId)) + " min");

        return view;
    }



    public static RestaurantFragment newInstance(long id) {
        RestaurantFragment rf = new RestaurantFragment();
        rf.restId = id;
        return rf;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_time:
                WaitParty wp = WaitParty.newInstance(restId);
                wp.show(getFragmentManager(), "waitParty");
                break;
        }
    }

    public interface RestaurantInterface { }
}