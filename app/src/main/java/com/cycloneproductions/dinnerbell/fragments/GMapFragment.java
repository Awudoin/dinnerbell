package com.cycloneproductions.dinnerbell.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cycloneproductions.dinnerbell.R;
import com.cycloneproductions.dinnerbell.model.GoogleMapPlace;
import com.cycloneproductions.dinnerbell.sqlite.DBHelper;
import com.cycloneproductions.dinnerbell.utilities.GooglePlacesHelper;
import com.cycloneproductions.dinnerbell.utilities.GooglePlacesHelper.GooglePlacesHelperCallbacks;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;

public class GMapFragment extends Fragment implements OnMapReadyCallback, GooglePlacesHelperCallbacks {
    private MapView mapView;
    private GoogleMap map;
    private DBHelper db;

    private HashMap<String, Marker> markers;

    public static GMapFragment newInstance() {
        GMapFragment fragment = new GMapFragment();
        fragment.markers = new HashMap<>();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBHelper(this.getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_map, container, false);
        mapView = (MapView)v.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    public String placeMarker(LatLng latLng, String title) {
        return placeMarker(new MarkerOptions().position(latLng).title(title));
    }

    public String placeMarker(MarkerOptions options) {
        Marker marker = map.addMarker(options);
        markers.put(marker.getId(), marker);
        return marker.getId();
    }

    public void removeMarker(String markerId) {
        if (markers.containsKey(markerId)) {
            markers.get(markerId).remove();
            markers.remove(markerId);
        }
    }

    public void removeAllMarkers() {
        for (String key : markers.keySet()) {
            markers.get(key).remove();
        }
        markers.clear();
    }

    public void moveCamera(LatLng to) {
        moveCamera(to, true);
    }

    public void moveCamera(LatLng to, boolean animate) {
        if (animate) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(to, 13));
        } else {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(to, 13));
        }
    }

    public void markRestaurantsInArea(LatLng latLng) {
        markRestaurantsInArea(latLng, true);
    }

    public void markRestaurantsInArea(LatLng latLng, boolean clearOld) {
        if (clearOld) {
            removeAllMarkers();
        }
        GooglePlacesHelper helper = new GooglePlacesHelper(this.getActivity(), this);
        helper.getPlaces(latLng, 5000);
    }

    @Override
    public void onGooglePlacesRetrieved(List<GoogleMapPlace> places) {
        db.clearAllRests();
        for (GoogleMapPlace place : places) {
            db.addRest(place);
            placeMarker(place.getLatLng(), place.getName());
        }
    }
}