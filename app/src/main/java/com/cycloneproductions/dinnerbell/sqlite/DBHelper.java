package com.cycloneproductions.dinnerbell.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cycloneproductions.dinnerbell.model.GoogleMapPlace;
import java.util.Date;

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 4;              //database version
    private static final String DATABASE_NAME = "DinnerDB";     //database name
    private static final String TABLE_REST = "restaurants";     //restaurants table name
    private static final String TABLE_TIMES = "times";          //times table name
    //columns
    public static final String KEY_ID = "_id";
    private static final String KEY_GID = "gid";
    private static final String KEY_DATE = "date";
    private static final String KEY_MIN = "minutes";
    private static final String KEY_PARTY = "party";
    private static final String KEY_REST_ID = "rest_id";

    private static final String KEY_LAT = "lat";
    private static final String KEY_LONG = "long";
    private static final String KEY_ICON = "icon";
    public static final String KEY_NAME = "name";
    public static final String KEY_ADDRESS = "address";

    private static final String[] REST_COLUMNS = {KEY_ID,KEY_GID,KEY_LAT,KEY_LONG,KEY_ICON,KEY_NAME,KEY_ADDRESS};
    private static final String[] TIMES_COLUMNS = {KEY_ID,KEY_REST_ID,KEY_DATE,KEY_MIN,KEY_PARTY};

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //sql statement to create table
        String CREATE_RESTAURANT_TABLE = "CREATE TABLE restaurants ( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "gid TEXT, " +
                "lat REAL, " +
                "long REAL, " +
                "icon TEXT, " +
                "name TEXT, " +
                "address TEXT);";
        //create table
        db.execSQL(CREATE_RESTAURANT_TABLE);

        String CREATE_TIME_TABLE = "CREATE TABLE times ( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "rest_id INT, " +
                "date TEXT, " +
                "minutes INT, " +
                "party INT);";
        db.execSQL(CREATE_TIME_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //drop older tables table if existed
        db.execSQL("DROP TABLE IF EXISTS dbell");
        db.execSQL("DROP TABLE IF EXISTS restaurants");
        db.execSQL("DROP TABLE IF EXISTS times");

        //create fresh dbell table
        this.onCreate(db);
    }

    public void addRest(GoogleMapPlace rest) {
        //for logging
        Log.d("addRest", rest.toString());

        //get database
        SQLiteDatabase db = this.getWritableDatabase();

        //creating values and key
        ContentValues values = new ContentValues();
        values.put(KEY_GID, rest.getPlaceId());
        values.put(KEY_NAME, rest.getName());
        values.put(KEY_ICON, rest.getIconPath());
        values.put(KEY_ADDRESS, rest.getFormattedAddress());

        //inserting into table
        long id = db.insert(TABLE_REST, null, values);
        //close

        db.close();
    }

    public double getAvgWaitTime(long restId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT avg(" + KEY_MIN + ") AS avg FROM " + TABLE_TIMES + " WHERE " + KEY_REST_ID + "=" + String.valueOf(restId) + ";", null);
        //if we got results get the first one
        if (cursor != null) {
            cursor.moveToFirst();
            return cursor.getDouble(cursor.getColumnIndex("avg"));
        } else {
            return 0.0;
        }
    }

    public void addTime(int restaurantId, int waitTime, int partySize, Date date) {
        Log.d("addTime", String.valueOf(restaurantId));

        //get database
        SQLiteDatabase db = this.getWritableDatabase();

        //creating values and key
        ContentValues values = new ContentValues();
        values.put(KEY_REST_ID, restaurantId);
        values.put(KEY_MIN, waitTime);
        values.put(KEY_PARTY, partySize);
        values.put(KEY_DATE, String.valueOf(date));

        //inserting into table
        db.insert(TABLE_TIMES, null, values);
        //close
        db.close();
    }

    public Cursor getAllRests() {
        //get database
        SQLiteDatabase db = this.getReadableDatabase();

        //build query
        Cursor cursor =
                db.query(TABLE_REST,    //table
                        REST_COLUMNS,   //columns
                        null,           //selection
                        null,           //selections
                        null,           //group by
                        null,           //having
                        null,           //order by
                        null);          //limit

        return cursor;
    }

    public void clearAllRests() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_REST);
    }


    public GoogleMapPlace getRest(long id) {
        //get database
        SQLiteDatabase db = this.getReadableDatabase();

        //build query
        Cursor cursor =
                db.query(TABLE_REST,                        //table
                        REST_COLUMNS,                       //columns
                        " _id = ?",                         //selection
                        new String[] { String.valueOf(id) },//selections
                        null,                               //group by
                        null,                               //having
                        null,                               //order by
                        null);                              //limit

        //if we got results get the first one
        if (cursor != null) {
            cursor.moveToFirst();
        }

        //build restaurant object
        GoogleMapPlace rest = new GoogleMapPlace();
        rest.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
        rest.setPlaceId(cursor.getString(cursor.getColumnIndex(KEY_GID)));
        rest.setFormattedAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
        rest.setIconPath(cursor.getString(cursor.getColumnIndex(KEY_ICON)));

        //log
        Log.d("getRest(" + id + ")",rest.toString());

        return rest;
    }
}