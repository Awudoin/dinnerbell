package com.cycloneproductions.dinnerbell.utilities;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.cycloneproductions.dinnerbell.R;
import com.cycloneproductions.dinnerbell.model.GoogleMapPlace;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GooglePlacesHelper {

    private Context context;
    private GooglePlacesHelperCallbacks callback;
    private ArrayList<GoogleMapPlace> tempPlaces;
    private String pageToken;
    private LatLng lastLatLng;
    private int lastRadius;

    public GooglePlacesHelper(Context context, GooglePlacesHelperCallbacks callback) {
        this.context = context;
        this.callback = callback;
    }

    private String constructUrlString() {
        String urlString;
        if (pageToken == null) {
            urlString = "https://maps.googleapis.com/maps/api/place/textsearch/json?"
                    + "query=restaurants"
                    + "&location=" + String.valueOf(lastLatLng.latitude) + "," + String.valueOf(lastLatLng.longitude)
                    + "&radius=" + String.valueOf(lastRadius)
                    + "&types=restaurant"
                    + "&sensor=true"
                    + "&key=" + context.getResources().getString(R.string.google_place_search_key);
        } else {
            urlString = "https://maps.googleapis.com/maps/api/place/textsearch/json?"
                    + "key=" + context.getResources().getString(R.string.google_place_search_key)
                    + "&pagetoken=" + pageToken;
        }

        return urlString;
    }

    public void getPlaces(LatLng latLng, int radius) {
        tempPlaces = new ArrayList<>();
        pageToken = null;

        lastLatLng = latLng;
        lastRadius = radius;

        Downloader downloader = new Downloader();
        downloader.execute();
    }

    private class Downloader extends AsyncTask<Void, Integer, String> {

        @Override
        protected String doInBackground(Void... params) {
            String data = null;
            try {
                data = downloadUrl();
            } catch (IOException e) {
                Log.d("Exception downloading: ", e.toString());
                e.printStackTrace();
            }
            return data;
        }

        private String downloadUrl() throws IOException {
            String data = "";
            InputStream inputStream = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(constructUrlString());

                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                inputStream = connection.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder buffer = new StringBuilder();

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                data = buffer.toString();
                reader.close();
            } catch (Exception e) {
                Log.d("Exception downloading", e.toString());
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (connection != null) {
                    connection.disconnect();
                }
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            Parser parser = new Parser();
            parser.execute(result);
        }
    }

    private class Parser extends AsyncTask<String, Integer, List<GoogleMapPlace>> {

        JSONObject jsonObject;

        @Override
        protected List<GoogleMapPlace> doInBackground(String... params) {
            List<GoogleMapPlace> places = null;

            try {
                jsonObject = new JSONObject(params[0]);
                places = parse(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return places;
        }

        private List<GoogleMapPlace> parse(JSONObject jsonObject) {
            JSONArray places = null;

            try {
                if (jsonObject.has("next_page_token")) {
                    pageToken = jsonObject.getString("next_page_token");
                } else {
                    pageToken = null;
                }

                places = jsonObject.getJSONArray("results");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getPlaces(places);
        }

        private List<GoogleMapPlace> getPlaces(JSONArray jsonArray) {
            int placesCount = jsonArray.length();

            List<GoogleMapPlace> places = new ArrayList<>(placesCount);

            for (int i = 0; i < placesCount; i++) {
                try {
                    places.add(getPlace((JSONObject) jsonArray.get(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return places;
        }

        private GoogleMapPlace getPlace(JSONObject placeData) {
            GoogleMapPlace place = new GoogleMapPlace();

            try {
                place.setName(placeData.isNull("name") ? "N/A" : placeData.getString("name"));
                //place.setVicinity(placeData.isNull("vicinity") ? "N/A" : placeData.getString("vicinity"));
                place.setFormattedAddress(placeData.isNull("formatted_address") ? "N/A" : placeData.getString("formatted_address"));
                place.setLatLng(placeData.getJSONObject("geometry").getJSONObject("location").getString("lat"), placeData.getJSONObject("geometry").getJSONObject("location").getString("lng"));
                place.setPlaceId(placeData.isNull("place_id") ? "N/A" : placeData.getString("place_id"));
                place.setIconPath(placeData.isNull("icon") ? "N/A" : placeData.getString("icon"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return place;
        }

        @Override
        protected void onPostExecute(List<GoogleMapPlace> result) {
            tempPlaces.addAll(result);
/*
            if (pageToken != null) {
                // Need to wait a little over a second for the next page token to become active on Google
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Downloader downloader = new Downloader();
                        downloader.execute();
                    }
                }, 1250);
            } else {*/
                callback.onGooglePlacesRetrieved(tempPlaces);
            //}
        }
    }

    public interface GooglePlacesHelperCallbacks {
        void onGooglePlacesRetrieved(List<GoogleMapPlace> places);
    }
}