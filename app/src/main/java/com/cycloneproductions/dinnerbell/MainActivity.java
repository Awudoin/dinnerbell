package com.cycloneproductions.dinnerbell;

import android.app.FragmentTransaction;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.cycloneproductions.dinnerbell.fragments.GMapFragment;
import com.cycloneproductions.dinnerbell.fragments.RestaurantFragment;
import com.cycloneproductions.dinnerbell.fragments.RestaurantListFragment;
import com.cycloneproductions.dinnerbell.fragments.SearchBarFragment;
import com.cycloneproductions.dinnerbell.fragments.SortBarFragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

public class MainActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks,
                                                             GoogleApiClient.OnConnectionFailedListener,
                                                             View.OnClickListener,
                                                             SearchBarFragment.SearchBarInterface,
                                                            RestaurantListFragment.ListInterface{

    GMapFragment mapFragment;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    SearchBarFragment searchBar;
    SortBarFragment sortBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        map_view();

        // Setup button click listeners
        findViewById(R.id.btn_map).setOnClickListener(this);
        findViewById(R.id.btn_list).setOnClickListener(this);
        findViewById(R.id.btn_fav).setOnClickListener(this);
        findViewById(R.id.btn_setting).setOnClickListener(this);

        buildGoogleApiClient();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_map:
                map_view();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        centerMapAndSearchRestaurants();
                    }
                }, 2000);
                break;
            case R.id.btn_list:
                list_view();
                break;
            case R.id.btn_fav:
                favorite_view();
                break;
            case R.id.btn_setting:
                settings_view();
                break;
        }
    }

    public void map_view() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (mapFragment == null) {
            mapFragment = GMapFragment.newInstance();
            transaction.replace(R.id.container, mapFragment);
        }
        if (searchBar == null) {
            searchBar = SearchBarFragment.newInstance();
            transaction.replace(R.id.top_bar, searchBar);
        }
        transaction.commit();
    }

    public void list_view() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        if (sortBar == null) {
            sortBar = SortBarFragment.newInstance();
            transaction.replace(R.id.top_bar, sortBar);
        }
        transaction.replace(R.id.container, RestaurantListFragment.newInstance());

        mapFragment = null;

        transaction.commit();
    }

    public void favorite_view() {
        //// TODO: flip the view to the favorite view if not favorite view
        //// TODO: only for the list view
    }

    public void settings_view() {
        //// TODO: show a settings view
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(Bundle bundle) {
        centerMapAndSearchRestaurants();
    }

    private void centerMapAndSearchRestaurants() {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            LatLng userLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            //LatLng userLocation = new LatLng(33, -112);

            mapFragment.markRestaurantsInArea(userLocation);
            mapFragment.moveCamera(userLocation);
        }
    }

    @Override
    public void onConnectionSuspended(int i) { }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, "TURN ON GPS...", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSearchClick(String searchText) {
        Toast.makeText(this, "SEARCHING....", Toast.LENGTH_SHORT).show();
        Geocoder geocoder = new Geocoder(this);
        try {
            List<Address> addressList = geocoder.getFromLocationName(searchText, 1);

            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

            mapFragment.markRestaurantsInArea(latLng);
            mapFragment.placeMarker(latLng, searchText);
            mapFragment.moveCamera(latLng);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void restSelected(long id) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, RestaurantFragment.newInstance(id));
        transaction.remove(sortBar);
        transaction.addToBackStack(null);
        transaction.commit();

        sortBar = null;
    }
}