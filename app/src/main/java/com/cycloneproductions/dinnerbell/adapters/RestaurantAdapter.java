package com.cycloneproductions.dinnerbell.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.cycloneproductions.dinnerbell.R;
import com.cycloneproductions.dinnerbell.sqlite.DBHelper;

public class RestaurantAdapter extends CursorAdapter {
    private final LayoutInflater inflater;
    private final DBHelper db;

    public RestaurantAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
        inflater = LayoutInflater.from(context);
        db = new DBHelper(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.restaurant_row, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        long restId = cursor.getLong(cursor.getColumnIndex(DBHelper.KEY_ID));

        TextView textName = (TextView) view.findViewById(R.id.textRestName);
        int nameColumnIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
        textName.setText(cursor.getString(nameColumnIndex));

        TextView wait = (TextView) view.findViewById(R.id.textRestWaitTime);
        wait.setText(String.valueOf(db.getAvgWaitTime(restId)) + " min");

    }
}