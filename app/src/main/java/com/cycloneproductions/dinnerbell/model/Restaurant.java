package com.cycloneproductions.dinnerbell.model;

public class Restaurant {

    private int id;
    private String gid;
    private String date;
    private int minutes;
    private int party;

    public Restaurant() { }

    public Restaurant(int id, String gid, String date, int minutes, int party) {
        this.id = id;
        this.gid = gid;
        this.date = date;
        this.minutes = minutes;
        this.party = party;
    }

    public int getParty() { return party; }
    public void setParty(int party) { this.party = party; }

    public int getMinutes() { return minutes; }
    public void setMinutes(int minutes) { this.minutes = minutes; }

    public String getDate() { return date; }
    public void setDate(String date) { this.date = date; }

    public String getGid() { return gid; }
    public void setGid(String gid) { this.gid = gid; }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
}