package com.cycloneproductions.dinnerbell.model;

import com.google.android.gms.maps.model.LatLng;

public class GoogleMapPlace {
    private LatLng latLng;
    private String iconPath;
    private String name;
    private String placeId;
    private String formattedAddress;

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public void setLatLng(double lat, double lng) {
        setLatLng(new LatLng(lat, lng));
    }

    public void setLatLng(String lat, String lng) {
        setLatLng(Double.valueOf(lat), Double.valueOf(lng));
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }
}
