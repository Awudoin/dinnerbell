package com.cycloneproductions.dinnerbell.submit;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.cycloneproductions.dinnerbell.R;
import com.cycloneproductions.dinnerbell.sqlite.DBHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WaitParty extends DialogFragment {

    DBHelper db;
    private long restId;

    public static WaitParty newInstance(long id) {
        WaitParty wp = new WaitParty();
        wp.restId = id;
        return wp;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.time_popup, null);

        final NumberPicker wait = (NumberPicker) view.findViewById(R.id.min_picker);
        final NumberPicker party = (NumberPicker) view.findViewById(R.id.party_picker);

        builder.setView(view)
                .setTitle(R.string.submit_time)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        submitWaitParty((wait.getValue() + 1) * 5, party.getValue());
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        //party picker
        party.setMinValue(0);
        party.setMaxValue(10);

        //wait picker
        String waitTimes[] = {"5","10","15","20","25","30","35","40","45","50","55","60"};
        wait.setMinValue(0);
        wait.setMaxValue(waitTimes.length-1);
        int min = wait.getMinValue();
        int max = wait.getMaxValue();
        wait.setDisplayedValues(waitTimes);
        int min2 = wait.getMinValue();
        int max2 = wait.getMaxValue();

        //return the dialogue for display
        return builder.create();
    }

    public void submitWaitParty (int minutes, int party) {
        db = new DBHelper(this.getActivity());
        db.getWritableDatabase();

        //setting todays date
        DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd hhmmss");
        dateFormatter.setLenient(false);
        Date today = new Date();

        //adding to db and closing
        db.addTime((int) restId,minutes,party,today);
        db.close();
    }
}